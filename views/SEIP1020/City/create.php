<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select City</h2>

    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Enter your name : </label> <br>
            <input class="input-group" type="text" name="name"><br>
            <label for="sel1">Select your city:</label>
            <select class="form-control" id="sel1" name="city">
                <option>Dhaka</option>
                <option>Chittagong</option>
                <option>Khulna</option>
                <option>Barisal</option>
                <option>Sylhet</option>
                <option>Rangpur</option>
                <option>Rajshahi</option>
            </select>
            <br>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>

</body>
</html>

