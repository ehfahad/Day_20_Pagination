<?php
namespace App\Bitm\SEIP1020\City;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class City
{
    public $id = "";
    public $name = "";
    public $city = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        if (array_key_exists("city", $data)) {
            $this->city = $data['city'];
        }

        //echo  $this;

    }


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }


    public function store(){
        $query = "INSERT INTO `atomicprojectb22`.`cities` (`name`, `city`) VALUES ('".$this->name."', '".$this->city."')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }
}