<?php
namespace App\Bitm\SEIP1020\Book;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class Book
{
    public $id = "";
    public $title = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("title", $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        //echo  $this;

    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`book` (`title`) VALUES ('" . $this->title . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allBook = array();
        $query = "SELECT * FROM `book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;


    }

    public function view()
    {
        $query = "SELECT * FROM `book` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectb22`.`book` SET `title` = '" . $this->title . "' WHERE `book`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`book` SET `deleted_at` =" . $this->deleted_at . " WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allBook = array();
        $query = "SELECT * FROM `book` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`book` SET `deleted_at` = NULL WHERE `book`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`book` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }




}